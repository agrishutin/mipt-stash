#include <termios.h>
#include <unistd.h>
#include <locale.h>
#include <stdlib.h>
#include <wchar.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <sys/ioctl.h>
#include <termcap.h>
#include <signal.h>
#include <ctype.h>

#define UNICODE
#ifdef UNICODE
#define _UNICODE
#else
#define _MBCS
#endif

#define CHUNK 16

#define wendl (wchar_t)(L'\n')
#define wspace (wchar_t)(L' ')
#define wnullterminator (wchar_t)(L'\0')
#define wtilda (wchar_t)(L'~')
#define wtab (wchar_t)(L'\t')

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

enum EVALUATION_RESULTS {SUCCESS,
                         FAIL,
                         EXIT};

struct winsize terminal_window;
int nflag = 0;

size_t linesn, max_str_len;
size_t max_linen_len;
size_t terminal_lines, terminal_columns;
size_t bound_line, bound_column;

wchar_t **content;

void printwc(wchar_t c)
{
  putwc((c), stdout);
}

size_t wstrlen(const wchar_t *s)
{
  size_t ans = 0;
  size_t i = 0;
  for (i = 0; s[i] != wnullterminator && s[i] != wendl; i++)
  {
    ans++;
  }
  return ans;
}

size_t getTerminalWidth()
{
  return terminal_window.ws_col;
}

size_t getTerminalHeight()
{
  return terminal_window.ws_row;
}

int readString(FILE *in, wchar_t **str)
{
  wchar_t *tmp = NULL;
  wint_t ch;
  size_t len = 0, buf_len = 100;
  size_t i;

  tmp = calloc(buf_len, sizeof(wchar_t));
  if (tmp == NULL)
  {
    perror("calloc");
    return FAIL;
  }
  *str = tmp;
  while (((ch = fgetwc(in)) != WEOF))
  {
    if (ch == wendl || ch == wnullterminator)
    {
      break;
    }
    if (ch == wtab)
    {
      buf_len += 3;
      tmp = realloc(*str, sizeof(wchar_t) * buf_len);
      if (tmp == NULL)
      {
        perror("realloc");
        return FAIL;
      }
      *str = tmp;
      for (i = 0; i < 4; i++)
      {
        (*str)[len] = wspace;
        len++;
      }
    }
    else
    {
      (*str)[len] = (wchar_t)ch;
      len++;
    }

    if(len == buf_len)
    {
      buf_len += CHUNK;
      tmp = realloc(*str, sizeof(wchar_t) * buf_len);
      if(tmp == NULL)
      {
        perror("realloc");
        return FAIL;
      }
      *str = tmp;
    }
  }

  (*str)[len] = wnullterminator;
  if (len > max_str_len)
  {
    max_str_len = len;
  }

  len++;
  tmp = realloc(*str, sizeof(wchar_t) * len);
  if (tmp == NULL)
  {
    perror("realloc");
    return FAIL;
  }
  *str = tmp;
  return SUCCESS;
}


int readFile(FILE *in, wchar_t ***text)
{
  wchar_t *cur_str;
  wchar_t **tmp;

  size_t len = 0, buf_len = 100;
  int status;

  tmp = calloc(buf_len, sizeof(wchar_t*));

  if(tmp == NULL)
  {
    perror("calloc");
    return FAIL;
  }
  *text = tmp;

  while (!(feof(in)))
  {
    status = readString(in, &cur_str);
    if (status == FAIL)
    {
      free(cur_str);
      return FAIL;
    }
    (*text)[len] = cur_str;
    len++;
    if(len == buf_len)
    {
      buf_len += CHUNK;
      tmp = realloc(*text, sizeof(wchar_t*) * buf_len);
      if (tmp == NULL)
      {
        perror("realloc");
        return FAIL;
      }
      *text = tmp;
    }
  }
  linesn += len;
  tmp = realloc(*text, sizeof(wchar_t*) * len);
  if (tmp == NULL)
  {
    perror("realloc");
    return FAIL;
  }
  *text = tmp;
  return SUCCESS;
}

struct termios old_attributes, new_attributes;

void reInitTerminalSizes()
{
  ioctl(0, TIOCGWINSZ, &terminal_window);
  terminal_lines = getTerminalHeight();
  terminal_columns = getTerminalWidth();
}

void setup_terminal()
{
  reInitTerminalSizes();
  tcgetattr(0, &old_attributes);
  memcpy(&new_attributes, &old_attributes, sizeof(struct termios));
  new_attributes.c_lflag &= ~ECHO;
  new_attributes.c_lflag &= ~ICANON;
  new_attributes.c_cc[VMIN] = 1;
  /*
  *    new_attributes.c_cc[VTIME] = 0;
  */
  tcsetattr(0, TCSANOW, &new_attributes);

  bound_line = 0;
  bound_column = 0;

}

void reset_terminal()
{
  tcsetattr(0,TCSANOW,&old_attributes);
}

size_t digits(size_t num)
{
  if (num == 0)
  {
    return 1;
  }
  size_t ans = 0;
  while (num > 0)
  {
    ans++;
    num /= 10;
  }
  return ans;
}

int toStr(size_t num, wchar_t **res)
{
  size_t len, buf_len = 3;
  size_t new_size;
  size_t i;
  int status;
  wchar_t tmp_ch;

  wchar_t *tmp;

  tmp = calloc(buf_len, sizeof(wchar_t));

  if (tmp == NULL)
  {
    perror("toStr memory allocation error");
    return FAIL;
  }
  *res = tmp;

  len = 0;
  while (num > 0) /* we dont handle 0 case, because string numbers start with 1*/
  {
    if (len == buf_len)
    {
      buf_len++;
      new_size = buf_len * sizeof(wchar_t);
      tmp = realloc(*res, new_size);
      if (tmp == NULL)
      {
        perror("realloc");
        return FAIL;
      }
      *res = tmp;
    }
    (*res)[len] = (wchar_t)((wchar_t)(L'0') + (num % 10));
    len++;
    num /= 10;
  }

  (*res)[len] = wnullterminator;
  for (i = 0; i < (len + 1) / 2; i++)
  {
    tmp_ch = (*res)[i];
    (*res)[i] = (*res)[len - i - 1];
    (*res)[len - i - 1] = tmp_ch;
  }
  return SUCCESS;
}

int rePrintText()
{
  size_t linen;
  size_t cur_dig;
  size_t i;
  size_t digits_bound = 0;
  wchar_t *cur_num_str;

  int status;

  max_linen_len = 0;
  if (nflag) /* additional UI elements are printed */
  {
    digits_bound = 0;
    for (linen = bound_line; linen < bound_line + terminal_lines; linen++)
    {
      cur_dig = digits(linen);
      digits_bound = (cur_dig > digits_bound?cur_dig:digits_bound);
      max_linen_len = digits_bound + 2;
    }
    for (linen = bound_line; (linen < bound_line + terminal_lines - 1) && (linen < linesn); linen++)
    {
      if (bound_column > 0)
      {
        printwc((wchar_t)(L'<'));
      }
      else
      {
        printwc((wchar_t)(L'|'));
      }

      status = toStr(linen + 1, &cur_num_str);
      if (status == FAIL)
      {
        free(cur_num_str);
        return FAIL;
      }

      cur_dig = wstrlen(cur_num_str);

      assert(digits_bound - cur_dig >= 0);
      assert(cur_dig >= 0);

      for (i = 0; i < digits_bound - cur_dig; i++)
      {
        printwc(wspace);
      }

      for (i = 0; i < cur_dig; i++)
      {
        printwc(cur_num_str[i]);
      }

      printwc((wchar_t)(L':'));

      for (i = bound_column; (i < wstrlen(content[linen])) && (2 + digits_bound + (i - bound_column) < terminal_columns); i++)
      //< 23: - 2 + digits_bound symbols
      {
        printwc(content[linen][i]);
      }
      printwc(wendl);
      free(cur_num_str);
    }
    for (; linen < bound_line + terminal_lines - 1; linen++)
    {
      printwc(wtilda);
      printwc(wendl);
    }
  }
  else /* raw output mode */
  {
    for (linen = bound_line; (linen < bound_line + terminal_lines - 1) && (linen < linesn); linen++)
    {
      for (i = bound_column; (i < wstrlen(content[linen])) && (i - bound_column < terminal_columns); i++)
      {
        printwc(content[linen][i]);
      }
      printwc(wendl);
    }
    for (; linen < bound_line + terminal_lines - 1; linen++)
    {
      printwc(wtilda);
      printwc(wendl);
    }
  }
  return SUCCESS;
}

void handle_winch(int sig)
{
  if (sig == SIGWINCH)
  {
    reInitTerminalSizes();
    rePrintText();
  }
  signal(SIGWINCH, handle_winch);
}

int isExitingCommand(wchar_t c)
{
  return c == 113; /* 'q' symbol */
}

int handle_terminal_command()
{
  int to_reprint = 0;
  int status;
  char c;

  c = fgetc(stdin);
  if (!(c))
  {
    return FAIL;
  }
  if (isExitingCommand(c))
  {
    return EXIT;
  }

  if (c == 27) /* special symbol was entered, need to read 2 more symbols from buffer to recognize which one */
  {
    c = fgetc(stdin);
    if (!(c))
    {
      return FAIL;
    }

    if (c != 91) /* some special symbol that we don't handle */
    {
      return SUCCESS;
    }
    c = fgetc(stdin);
    switch (c)
    {
      case 65: /* up arrow */
        if (bound_line > 0)
        {
          bound_line--;
          to_reprint = 1;
        }
        break;
      case 66: /* down arrow */
        if (bound_line + terminal_lines <= linesn)
        {
          bound_line++;
          to_reprint = 1;
        }
        break;
      case 68: /* left arrow */
       if (bound_column > 0)
       {
         bound_column--;
         to_reprint = 1;
       }
       break;
      case 67: /* right arrow */
        if (bound_column + terminal_columns < max_str_len + max_linen_len * nflag)
        {
          bound_column++;
          to_reprint = 1;
        }
        break;
      case 54: /* page_down */
        if (bound_line + terminal_lines - 1 <= (long int)linesn - terminal_lines)
        {
          bound_line = bound_line + terminal_lines - 1;
          to_reprint = 1;
        }
        c = fgetc(stdin); /* need to read some trash symbol */
        break;
      case 53: /* page_up */
        if (bound_line > 0)
        {
          to_reprint = 1;
          bound_line = max((long int)bound_line - (long int)terminal_lines + 1, 0);
        }
        c = fgetc(stdin);
        break;
    }
    if (to_reprint)
    {
      status = rePrintText();
      if (status == FAIL)
      {
        return EXIT;
      }
    }
    return SUCCESS;
  }
  return SUCCESS; /* not control command was printed */
}

void free_all()
{
  size_t i;
  for (i = 0; i < linesn; i++)
  {
    free(content[i]);
  }
  free(content);
}

int main(int argc, char* argv[])
{
  FILE *input;
  char *filename = NULL;

  int status;
  int c;
  size_t i;

  if(!isatty(0))
  {
      wprintf(L"Not stdin control detected. Exiting.\n");
      return 1;
  }

  setlocale(LC_ALL, /*"ru_RU.UTF-8"*/ "");

  if (argc < 2)
  {
    fwprintf(stderr, L"Usage: ./myless -f <filename> [-n]\n");
    return 1;
  }

  opterr = 0;
  while ((c = getopt (argc, argv, "nf:")) != -1)
  {
      switch (c)
        {
        case 'n':
          nflag = 1;
          break;
        case 'f':
          filename = optarg;
          break;
        default:
          fwprintf(stderr, L"Usage: ./myless -f <filename> [-n]\n");
          reset_terminal();
          return 1;
        }
  }

  if (filename == NULL)
  {
    fwprintf(stderr, L"Usage: ./myless -f <filename> [-n]\n");
    reset_terminal();
    return 1;
  }

  input = fopen(filename, "r");
  if (input == NULL)
  {
    fclose(input);
    reset_terminal();
    return 0;
  }

  linesn = 0;
  status = readFile(input, &content);

  if (status == FAIL)
  {
    free_all();
    fclose(input);
    reset_terminal();
  }

  signal(SIGWINCH, handle_winch);

  setup_terminal();
  status = rePrintText();
  if (status == FAIL)
  {
    free_all();
    fclose(input);
    reset_terminal();
  }

  for (; handle_terminal_command() == SUCCESS;); /* loop while exit-symbol not entered */

  free_all();
  reset_terminal();
  fclose(input);
  return 0;
}
