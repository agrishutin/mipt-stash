#include <time.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdlib.h>

#define MEDCHST '+'
#define PSNCHST '-'
#define WALL '#'
#define PASSAGE ' '
#define PLAYER 'Y'

#define DEFAULT_PORT "3490"

typedef struct map {
  size_t width;
  size_t height;
  char **map;
} map_t;

typedef struct game_info {
  map_t *map;
  int player_health;
  int active_health_loss;
  int passive_health_loss;
  int bomb_range;


  struct tm *start_timeout;
} game_t;

typedef enum server_states {
  ST_HOST_WAIT,
  ST_HOST_JOINED,
  ST_GAME_STARTED,
  ST_SERVER_ERROR
} server_state;

typedef enum client_states {
  ST_SERVER_CONN_WAIT,
  ST_CONNECTED,
  ST_PLAYING
} client_state;
