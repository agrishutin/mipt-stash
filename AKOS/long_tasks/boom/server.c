#include "utility.h"

int main(int argc  __attribute__((unused)), char *argv[]  __attribute__((unused)))
{
  struct sockaddr_in serv_addr;

	/* initialize socket structure */
	bzero((char*) &serv_addr, sizeof( serv_addr ));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons((uint16_t) port_num );

	/* bind the socket*/
	CHN1( bind( listener_fd, (struct sockaddr*) &serv_addr, sizeof( serv_addr )), 21, "Can't bind socket" );

	/* listen socket */
	CHN1( listen( listener_fd, BACKLOG ), 29, "Can't listen" );
  return 0;
}
