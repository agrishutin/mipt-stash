#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

#define true 1
#define false 0

#define free_all() \
				if (cur_acid != NULL) \
				{\
					free(cur_acid);\
				}\
				if (cur_encoding != NULL)\
				{\
					free(cur_encoding);\
				}\
				if (cur_name != NULL)\
				{\
					free(cur_name);\
				}

/* We assume that our alphabet consists of number of letters
 * that is given is ALPHABET_SIZE constant
*/
const size_t ALPHABET_SIZE = 6;
const size_t BITS_FOR_ENCODING = 4;
const size_t BITS_IN_INT = sizeof(int) * 8;
const size_t MAX_SEQUENCE_NAME_LENGTH = 300;
const size_t MAX_ACID_LENGTH = 5000000;
const size_t NUCLEOTIDS_STORED_IN_INT = sizeof(int) * 8 / 4;

const int NUCL_READING_MASK = 15; /* = 0b1111 */

const char ALPHABET[] = {'\0', 'a', 't', 'g', 'c', 'u'};

enum EVALUATION_ERRORS { SUCCESS,
						 INCORRECT_SYMBOL,
						 LACK_OF_MEMORY,
						 FILE_ERROR,
						 WRONG_INPUT,
						 MEMORY_ALLOCATION_ERROR,
						 WRONG_ENCODING_FORMAT };

char lower(char c)
{
	if ('A' <= c && c <= 'Z')
	{
		return 'a' + (c - 'A');
	}
	else
	{
		return c;
	}
}

char in_alphabet (char c)
{
	size_t i = 0;
	for (; (i < ALPHABET_SIZE) && ALPHABET[i] != c; i++);
	if (i == ALPHABET_SIZE)
	{
		return false;
	}
	else
	{
		return true;
	}
}

int encode_nucleotid(char nucl, char** buffer)
{
	size_t symbol_index = 0;
	size_t i;

	for (; (symbol_index < ALPHABET_SIZE) && (ALPHABET[symbol_index] != nucl); symbol_index++);
	if (symbol_index == ALPHABET_SIZE)
	{
		return INCORRECT_SYMBOL;
	}
	else
	{
		for (i = 0; i < BITS_FOR_ENCODING; i++)
		{
			buffer[0][i] = ((symbol_index & (1 << i))?'1':'0');
		}
	}
	return SUCCESS;
}

int pack(char** s, size_t size, int** res)
{
	size_t i, j;
	size_t cur_int, cur_bit;
	int status;
	char cur_nucl;
	char* cur_nucl_encoding;

	cur_int = 0;
	cur_bit = 0;
	for (j = 0; j < size; j++)
	{
		cur_nucl = (*s)[j];

		cur_nucl_encoding = calloc(BITS_FOR_ENCODING, sizeof(char));
		if (!(cur_nucl_encoding))
		{
			free(cur_nucl_encoding);
			printf("Encoding memory allocation");
			return MEMORY_ALLOCATION_ERROR;
		}
		status = encode_nucleotid(cur_nucl, &cur_nucl_encoding);
		if (status == INCORRECT_SYMBOL)
		{
			free(cur_nucl_encoding);
			return INCORRECT_SYMBOL;
		}
		else
		{
			for (i = 0; i < BITS_FOR_ENCODING; i++)
			{
				(*res)[cur_int] |= ((cur_nucl_encoding[i] == '1')?1:0) << cur_bit;
				cur_bit++;
			}
			if (cur_bit == BITS_IN_INT)
			{
				cur_int++;
				cur_bit = 0;
			}
		}
		free(cur_nucl_encoding);
	}
	return SUCCESS;
}

int unpack(int** s, size_t size, char** res)
{
	size_t i, cur_nucl_i, cur_bit, cur_nucl_number;
	int cur_int;
	char cur_nucl;

	cur_int = 0;
	cur_nucl_i = 0;
	for (i = 0; i < size; i++)
	{
		cur_int = s[0][i];
		for (cur_bit = 0; (cur_bit < BITS_IN_INT) && (cur_bit + BITS_FOR_ENCODING <= BITS_IN_INT); cur_bit += BITS_FOR_ENCODING)
		{
			cur_nucl_number = ((NUCL_READING_MASK << cur_bit) & (cur_int)) >> cur_bit;
			if (cur_nucl_number > ALPHABET_SIZE)
			{
				return WRONG_ENCODING_FORMAT;
			}
			cur_nucl = ALPHABET[cur_nucl_number];
			res[0][cur_nucl_i] = cur_nucl;
			cur_nucl_i++;
		}
	}

	return SUCCESS;
}

int PackToFile(FILE* source, FILE* target)
{
	int status;
	char cur_c;
	size_t cur_name_length, cur_acid_len, cur_nucl_index, required_ints, buf_len;

	char* tmp;
	char* cur_acid;
	char* cur_name;

	int* cur_encoding;

	cur_c = ' ';
	while (!(feof(source)))
	{
		if (cur_c != '>')
		{
			cur_c = fgetc(source);
		}
		while ((!(feof(source))) && (cur_c != '>'))
		{
			cur_c = fgetc(source);
		}

		if (!(cur_name = calloc(MAX_SEQUENCE_NAME_LENGTH, sizeof(char))))
		{
			free(cur_name);
			return MEMORY_ALLOCATION_ERROR;
		}

		cur_name_length = 0;
		buf_len = MAX_SEQUENCE_NAME_LENGTH;

		while ((!(feof(source))) && (cur_c = fgetc(source)) != '\n')
		{
			if (cur_name_length == buf_len)
			{
				buf_len *= 2;
				tmp = realloc(cur_name, buf_len * sizeof(char));
				if (tmp == NULL)
				{
					free(cur_name);
					return MEMORY_ALLOCATION_ERROR;
				}
				cur_name = tmp;
			}
			cur_name[cur_name_length] = cur_c;
			cur_name_length++;
		}

		if (cur_name_length == buf_len)
		{
			buf_len++;
			tmp = realloc(cur_name, buf_len * sizeof(char));
			if (tmp == NULL)
			{
				free(cur_name);
				return MEMORY_ALLOCATION_ERROR;
			}
			cur_name = tmp;
		}

		cur_name[cur_name_length] = '\0';
		cur_name_length++;

		cur_acid = calloc(MAX_ACID_LENGTH, sizeof(char));
		if (cur_acid == NULL)
		{
			free(cur_name);
			return MEMORY_ALLOCATION_ERROR;
		}

		fwrite(&cur_name_length, sizeof(size_t), 1, target);
		fwrite(cur_name, sizeof(char), cur_name_length, target);

		cur_acid_len = 0;
		buf_len = MAX_ACID_LENGTH;
		while ((!(feof(source)) && (cur_c = getc(source)) != '>'))
		{
			cur_c = lower(cur_c);
			cur_nucl_index = 0;
			while ((cur_nucl_index < ALPHABET_SIZE) && (ALPHABET[cur_nucl_index] != cur_c))
			{
				cur_nucl_index++;
			}
			if (cur_nucl_index == ALPHABET_SIZE)
			{
				continue;
			}
			else
			{
				if (cur_acid_len == buf_len)
				{
					buf_len *= 2;
					tmp = realloc(cur_acid, buf_len * sizeof(char));
					if (tmp == NULL)
					{
						free(cur_name);
						free(cur_acid);
						return MEMORY_ALLOCATION_ERROR;
					}
					cur_acid = tmp;
				}
				cur_acid[cur_acid_len] = cur_c;
				cur_acid_len++;
			}
		}

		required_ints = (cur_acid_len + NUCLEOTIDS_STORED_IN_INT - 1) / (NUCLEOTIDS_STORED_IN_INT);
		if (!(cur_encoding = calloc(required_ints, sizeof(int))))
		{
			free(cur_name);
			free(cur_acid);

			return MEMORY_ALLOCATION_ERROR;
		}
		tmp = realloc(cur_acid, cur_acid_len * sizeof(char));
		if (tmp == NULL)
		{
			free(cur_acid);
			free(cur_name);
			free(cur_encoding);
			return MEMORY_ALLOCATION_ERROR;
		}
		cur_acid = tmp;
		tmp = realloc(cur_name, cur_name_length * sizeof(char));
		if (tmp == NULL)
		{
			free(cur_acid);
			free(cur_name);
			free(cur_encoding);
			return MEMORY_ALLOCATION_ERROR;
		}
		cur_name = tmp;

		status = pack(&cur_acid, cur_acid_len, &cur_encoding);

		if (status == INCORRECT_SYMBOL)
		{
			free(cur_name);
			free(cur_encoding);
			free(cur_acid);

			return INCORRECT_SYMBOL;
		}
		else if (status == LACK_OF_MEMORY)
		{
			free(cur_name);
			free(cur_encoding);
			free(cur_acid);

			return LACK_OF_MEMORY;
		}
		else if (status == MEMORY_ALLOCATION_ERROR)
		{
			free(cur_name);
			free(cur_encoding);
			free(cur_acid);

			return MEMORY_ALLOCATION_ERROR;
		}
		else
		{
			fwrite(&required_ints, sizeof(size_t), 1, target);
			fwrite(cur_encoding, sizeof(int), required_ints, target);

			free(cur_name);
			free(cur_encoding);
			free(cur_acid);
		}
	}
	return SUCCESS;
}

int UnpackFromFile(FILE *source, FILE *target)
{
	int status;
	size_t i, j, cur_row, cur_name_length, required_ints, cur_acid_len, cur_nucl_i;

	char* cur_acid;
	char* cur_name;

	int* cur_encoding;

	while (!(feof(source)))
	{
		if ((fread(&cur_name_length, sizeof(size_t), 1, source) == 0))
		{
			break;
		}
		if (!(cur_name = calloc(cur_name_length + 1, sizeof(char))))
		{
			free(cur_name);

			printf("Name memory allocation");
			return MEMORY_ALLOCATION_ERROR;
		}

		fread(cur_name, sizeof(char), cur_name_length, source);
		cur_name[cur_name_length] = '\0';

		fread(&required_ints, sizeof(size_t), 1, source);
		if (!(cur_encoding = calloc(required_ints, sizeof(int))))
		{
			free(cur_name);
			free(cur_encoding);

			printf("Resulting ints memory allocation");
			return MEMORY_ALLOCATION_ERROR;
		}

		fread(cur_encoding, required_ints, sizeof(int), source);

		if (!(cur_acid = calloc(required_ints * NUCLEOTIDS_STORED_IN_INT + 1, sizeof(char))))
		{
			free(cur_name);
			free(cur_encoding);
			free(cur_acid);

			printf("Acid memory allocation");
			return MEMORY_ALLOCATION_ERROR;
		}

		status = unpack(&cur_encoding, required_ints, &cur_acid);
		if (status == WRONG_ENCODING_FORMAT)
		{
			free(cur_name);
			free(cur_encoding);
			free(cur_acid);
			return WRONG_ENCODING_FORMAT;
		}
		cur_acid[required_ints * NUCLEOTIDS_STORED_IN_INT] = '\0';

		fprintf(target, ">%s\n", cur_name);

		cur_acid_len = required_ints * NUCLEOTIDS_STORED_IN_INT;

		cur_nucl_i = 0;
		for (cur_row = 0; (cur_nucl_i < cur_acid_len) && (cur_acid[cur_nucl_i] != 0); cur_row++) {
			fprintf(target, "   %d ", 60 * (int)cur_row + 1);
			for (i = 0; i < 6; i++)
			{
				for (j = 0; (j < 10) && (cur_acid[cur_nucl_i] != 0); j++, cur_nucl_i++)
				{
					fprintf(target, "%c", cur_acid[cur_nucl_i]);
				}
				fprintf(target, " ");
			}
			fprintf(target, "\n");
		}
		free(cur_name);
		free(cur_encoding);
		free(cur_acid);
	}
	return SUCCESS;
}

/* can be runned either in coding or decoding mode with command
 * ./packer mode from to
 */
int main (int argc, char* argv[])
{
	int status;

	FILE * source;
	FILE * target;

	if (argc != 4)
	{
		printf(">Usage: ./packer <encode/decode> <source> <target>\n");

		return(FILE_ERROR);
	}

	source = fopen(argv[2], "r");
	if (source == NULL)
	{
		printf("Source file error(maybe no such file %s)\n", argv[2]);
		return(FILE_ERROR);
	}
	target = fopen(argv[3], "w");
	if (target == NULL)
	{
		printf("Target file error(maybe no such file %s)\n", argv[3]);
		return(FILE_ERROR);
	}

	if (strcmp(argv[1], "encode") == 0)	/* encode mode */
	{
		printf("Started encoding\n");
		status = PackToFile(source, target);
		if (status == SUCCESS)
		{
			printf("Success\n");
		}
		else if (status == MEMORY_ALLOCATION_ERROR)
		{
			printf("Memory allocation error\n");
		}
		else if (status == INCORRECT_SYMBOL)
		{
			printf("Incorrect symbol in input\n");
		}
		else if (status == LACK_OF_MEMORY)
		{
			printf("Not enough buffer size(m.b. lack of memory at heap)\n");
		}
	}
	else if (strcmp(argv[1], "decode") == 0)
	{
		printf("Started decoding\n");
		status = UnpackFromFile(source, target);
		if (status == SUCCESS)
		{
			printf("Success\n");
		}
		else if (status == MEMORY_ALLOCATION_ERROR)
		{
			printf("Memory allocation error\n");
		}
		else if (status == WRONG_ENCODING_FORMAT)
		{
			printf("Wrong encoding format\n");
		}
	}
	else
	{
		printf("Unknown parameter %s. Usage: ./packer <encode/decode> <source> <target>\n", argv[1]);
	}

	fclose(source);
	fclose(target);

	return 0;
}
