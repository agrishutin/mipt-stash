#include <stdio.h>
#include <stdlib.h>

const int START_BIT = 24, END_BIT = 29 + 1;

unsigned int modify(unsigned int num)
{
    unsigned int res = 0;
    size_t bitcnt = 0;
    size_t byten, i;
    for (byten = 0; byten < 2; byten++)
    {
        for (i = 0; i < 8; i++)
        {
            if (num & (1 << (8 * byten + i))) 
            {
                bitcnt++;
            }
            else
            {
                res |= (1 << (8 * byten + i));
            }
        }
    }

    for (; byten < 3; byten++)
    {
        for (i = 0; i < 8; i++)
        {
            res |= (num & (1 << (8 * byten + i)));
        }
    }

    for (i = 8 * byten; i < START_BIT; i++)
    {
        res |= (num & (1 << i));
    }

    for (i = START_BIT; i < END_BIT; i++)
    {
        res |= (bitcnt & (1 << (i - START_BIT))) << (START_BIT);
    }
    
    for (i = END_BIT; i % 8 != 0; i++)
    {
        res |= (num & (1 << i));
    }
    byten = (END_BIT + 8 - 1) / 8;
    
    for (; byten < sizeof(num); byten++)
    {
        for (i = 0; i < 8; i++) 
        {
            res |= (num & (1 << (8 * byten + i)));
        }
    }

    return res;
}

int main(int argc, char*argv[]) 
{
    if (argc != 2)
    {
        printf("Usage: ./modify <num>\n");
        return 1;
    }
    printf("%u\n", modify(atoi(argv[1])));
    return 0;
}