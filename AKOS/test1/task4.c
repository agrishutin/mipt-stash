#include <stdio.h>

const int float_bytes = sizeof(float);

int main(int argc, char*argv[]) 
{
    size_t curn;
    float a, b;
    FILE * fin;

    if (argc != 2)
    {
        printf("Usage: float_sort <filename>\n");
        return 1;
    }

    fin = fopen(argv[1], "r+");
    if (fin == NULL)
    {
        perror("File");
        return 2;
    }

    curn = 0;
    while (fread((void *)(&a), sizeof(float), 1, fin))
    {
        curn++;
	    if (!(fread((void *)(&b), sizeof(float), 1, fin)))
        {
            break;
        }

        curn++;
        if (a < b)
        {
            fseek(fin, -2 * sizeof(float), SEEK_CUR);
            fwrite((void *)(&b), sizeof(float), 1, fin);
            fwrite((void *)(&a), sizeof(float), 1, fin);
            if (curn > 2)
            {
                fseek(fin, -3 * sizeof(float), SEEK_CUR);
                curn -= 3;
            }
        }
        else 
        {
            fseek(fin, -1 * sizeof(float), SEEK_CUR);
            curn--;
        }
    }
    fclose(fin);
    return 0;
}
