#include <stdio.h>
#include <string.h>

enum INSERTION_ERRORS { SUCCESS,
						STRING_NOT_FOUND,
						NULLPTREXCEPTION };

struct listelm
{
	char *str;
	struct listelm *next;
};

size_t spaces_cnt (char **s)
{
	size_t s_len = strlen(s[0]);
	size_t i, ans = 0;
	for (i = 0; i < s_len; i++)
	{
		if (s[0][i] == ' ')
		{
			ans++;
		}
	}
	return ans;
}

int insert(struct listelm *start, char **strings)
{
	char *s1, *s2;
	struct listelm *prev_s, *cur_s;
	struct listelm e1, e2;
	int spacen;

	if (start == NULL)
	{
		return NULLPTREXCEPTION;
	}

	s1 = strings[0];
	s2 = strings[1];
	
	e1.str = s1;
	e1.next = NULL;
	e2.str = s2;
	e2.next = NULL;

	cur_s = start;
	prev_s = NULL;

	spacen = spaces_cnt(&s1);

	for (; cur_s->next; cur_s = cur_s->next)
	{
		if (spaces_cnt(&(cur_s->str)) == spacen)
		{
			if (prev_s == NULL)
			{
				e1.next = start->next;
				e2.next = cur_s;
				start = &e2;
			}
			else
			{
				e1.next = cur_s->next;
				cur_s->next = &e1;
				e2.next = cur_s;
				prev_s->next = &e2;
			}
			return SUCCESS;
		}
		prev_s = cur_s;
	}

	if (spaces_cnt(&(cur_s->str)) == spacen)
	{
		e1.next = NULL;
		cur_s->next = &e1;
		e2.next = cur_s;
		if (prev_s == NULL)
		{
			start = &e2;
		}
		else
		{
			prev_s->next = &e2;
		}
	}

	return STRING_NOT_FOUND; /* In couldnt find any string with same number of spaces as in strings[0] */
}

int main()
{
	return 0;
}
