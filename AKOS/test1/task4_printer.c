#include <stdio.h>

int main(int argc, char*argv[])
{
	FILE * source;
	float curfloat;

	if (argc != 2)
	{
		printf("Usage: ./print <filename>\n");
		return 1;
	}
	source = fopen(argv[1], "r");
	if (source == NULL)
	{
		perror("File");
		return 2;
	}

	while (fread((void*)(&curfloat), sizeof(float), 1, source))
	{
		printf("%f\n", curfloat);
	}
	printf("\n");
	fclose(source);
	return 0;
}