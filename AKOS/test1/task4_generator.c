#include <stdio.h>
#include <stdlib.h>

int main(int argc, char*argv[])
{
	FILE * target;
	int left;
	float curfloat;
	if (argc != 3)
	{
		printf("Usage: generator <floats_num> <target_filename>\n");
		exit(1);
	}
	target = fopen(argv[2], "w");
	if (target == NULL)
	{
		perror("File");
		exit(2);
	}

	left = atoi(argv[1]);
	while (left > 0)
	{
		scanf("%f", &curfloat);
		fwrite((void*)(&curfloat), sizeof(float), 1, target);
		left--;
	}
	fclose(target);
	return 0;
}
