#include <pwd.h>
#include <syslog.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/file.h>
#include <sys/types.h>
#include <stdio.h>

#include "misc.h"

key_t key;
int shmid = -1;
int semid = -1;

mem_t* data;
struct sembuf wait_client[1] = {{0, -1, 0}};
struct sembuf send_client[1] = {{1, 1, 0}};

size_t symbols_read;
size_t i;

FILE *client_file;
char *tmp;


void free_all()
{
  if (data != NULL) shmdt(&data);
  if (client_file != NULL) fclose(client_file);
  if (tmp != NULL) free(tmp);
  if (semid != -1) semctl(semid, 0, IPC_RMID);
}

void termination_handler(int sign __attribute__((unused)))
{
    free_all();
    exit(0);
}


int main(int argc, char *argv[])
{
    if (signal (SIGINT, termination_handler) == SIG_IGN)
      signal (SIGINT, SIG_IGN);
    if (signal (SIGHUP, termination_handler) == SIG_IGN)
      signal (SIGHUP, SIG_IGN);
    if (signal (SIGTERM, termination_handler) == SIG_IGN)
      signal (SIGTERM, SIG_IGN);

    /* now main code part starts */
    key = ftok(SHMEMPATH, SHMEMKEY);
    shmid = shmget(key, sizeof(mem_t), IPC_CREAT | 0666);
    if (shmid == -1)
    {
        perror("shared memory id get error");
        free_all();
        return 1;
    }

    semid = semget(key, NUMSEMS, IPC_CREAT | 0666);
    if (semid == -1)
    {
        printf("semaphors id get failed\n");
        free_all();
        return 1;
    }

    data = (mem_t*)shmat(shmid, NULL, 0);
    if (data == NULL)
    {
        printf("shmat failed\n");
        free_all();
        return 1;
    }

    if (argc < 2)
    {
      fprintf(stderr, "Usage: sender <filename>\n");
      free_all();
      return 1;
    }

    client_file = fopen(argv[1], "r");
    if (client_file == NULL)
    {
      perror("fopen");
      return 1;
    }

    while (1)
    {
        semop(semid, wait_client, 1);
        switch (data->pk_type)
        {
          /* client ready to accept */
          case PK_OK:
            memset(data->data, 0, BUF_SIZE);
            if (feof(client_file))
            {
                data->pk_type = PK_EOF;
                data->read_len = 0;
            }
            else
            {
                symbols_read = fread(data->data, sizeof(char), BUF_SIZE, client_file);
                data->read_len = symbols_read;

                if(feof(client_file))
                {
                     data->pk_type = PK_EOF;
                }
                else
                {
                     data->pk_type = PK_SEND_DATA;
                }
            }
            break;
          default:
            fprintf(stderr, "Unknown packet type\n");
            free_all();
            return 0;
            break;
          case PK_ERROR:
            fprintf(stderr, "Client sent error\n");
          case PK_FINISHED:
            free_all();
            return 0;
            break;
        }
        semop(semid, send_client, 1);
    }

    free_all();
    return 0;
}
