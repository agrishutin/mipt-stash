#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include "misc.h"

key_t key;
int shmid;
int semid;

mem_t* data;

size_t written = 0;
size_t content_size = 0;
size_t i, j;
size_t string_start_i;

int is_odd;
int has_error;

char *content;
char *tmp;

struct sembuf free_resource[1] = {{RES_FREE, 1, 0}};
struct sembuf wakeup_server[1] = {{0, 1, 0}};
struct sembuf wait_server[1] = {{1, -1, 0}};

size_t i;

void free_all()
{
  if (data != NULL) shmdt(data);
  if (content != NULL) free(content);
  if (tmp != NULL) free(tmp);
  if (semid != -1) semctl(semid, 0, IPC_RMID);
}

int IsENDL(char c)
{
  return (c == '\n');
}

int main()
{
    key = ftok(SHMEMPATH, SHMEMKEY);
    shmid = shmget(key, sizeof(mem_t), 0);
    semid = semget(key, NUMSEMS, 0);
    if ((shmid == -1) || (semid == -1))
    {
        perror("ipc get failed");
        return 1;
    }

    data = (mem_t*)shmat(shmid, NULL, 0);
    if (data == (mem_t*)(-1))
    {
        perror("shmat failed\n");
        return 2;
    }

    data->pk_type = PK_OK;
    semop(semid, wakeup_server, 1);
    semop(semid, wait_server, 1);
    if(data->pk_type == PK_ERROR)
    {
        fprintf(stderr, "server error %s\n", data->data);
        return 1;
    }

    while(data->pk_type == PK_SEND_DATA || data->pk_type == PK_EOF)
    {
      if (written + BUF_SIZE >= content_size)
      {
        printf("resize\n");
        content_size += BUF_SIZE;
        if (content == NULL)
        {
          tmp = calloc(BUF_SIZE, sizeof(char));
          if (tmp == NULL)
          {
            data->pk_type = PK_ERROR;
            perror("calloc");
            free_all();
            return 1;
          }
          content = tmp;
        }

        tmp = realloc(content, sizeof(char) * content_size);
        if (tmp == NULL)
        {
          data->pk_type = PK_ERROR;
          perror("realloc");
          free_all();
          return 1;
        }
        content = tmp;
      }
      printf("buf copy\n");
      assert(data->read_len <= BUF_SIZE);
      for (i = 0; i < data->read_len; i++)
      {
        content[written + i] = data->data[i];
      }
      written += data->read_len;
      printf("read total\n%s\n", content);
      if (data->pk_type == PK_EOF) break;

      data->pk_type = PK_OK;
      semop(semid, wakeup_server, 1);
      semop(semid, wait_server, 1);
    }

    if(data->pk_type == PK_ERROR)
    {
        fprintf(stderr, "server error\n");
        free_all();
        return 1;
    }

    if(data->pk_type == PK_FINISHED)
    {
        fprintf(stderr, "server finished working\n");
        free_all();
        return 1;
    }

    is_odd = 1;
    has_error = 0;
    string_start_i = 0;
    for (i = 0; i < written; i++)
    {
      if (content[i] == '#') has_error = 1;
      if (IsENDL(content[i]))
      {
        if (is_odd && has_error)
        {
          for (j = string_start_i; j <= i; j++)
          {
            fprintf(stderr, "%c\n", content[j]);
          }
        }
        string_start_i = i + 1;
        is_odd = 1 - is_odd;
      }
    }

    /* last line may not end with \n */
    if (is_odd && has_error)
    {
      for (j = string_start_i; j <= i; j++)
      {
        fprintf(stderr, "%c\n", content[j]);
      }
    }


    data->pk_type = PK_FINISHED;

    free_all();
    return 0;
}
