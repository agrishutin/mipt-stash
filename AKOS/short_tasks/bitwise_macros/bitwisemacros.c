#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define BITWISE_PRINT(number)                                                                                       \
{                                                                                                                   \
  int i, j, k, cur_bit, cur_hex_digit;                                                                              \
  int bytes_num = sizeof(number), bits = bytes_num * CHAR_BIT;                                                      \
                                                                                                                    \
  unsigned char* charwise = (unsigned char*)(&number);                                                              \
  char* bit_representation = malloc(bytes_num * CHAR_BIT);                                                          \
                                                                                                                    \
  for (i = bytes_num - 1; i >= 0; i--) {                                                                            \
    for (k = CHAR_BIT - 1; k >= 0; k--) {                                                                           \
      bit_representation[(bytes_num - i - 1) * CHAR_BIT + (CHAR_BIT - k - 1)] = ((charwise[i] & (1 << k))?'1':'0'); \
    }                                                                                                               \
  }                                                                                                                 \
  for (i = 0; i < bits;) {                                                                                          \
    for (j = 0; j < 2; j++) {                                                                                       \
      for (cur_bit = 0; cur_bit < 4; cur_bit++, i++) {                                                              \
        printf("%c", bit_representation[i]);                                                                        \
      }                                                                                                             \
      printf(" ");                                                                                                  \
    }                                                                                                               \
    printf("| ");                                                                                                   \
  }                                                                                                                 \
  printf("\n");                                                                                                     \
  for (i = 0; i < bits;) {                                                                                          \
    for (j = 0; j < 2; j++) {                                                                                       \
      cur_hex_digit = 0;                                                                                            \
      for (cur_bit = 0; cur_bit < 4; cur_bit++, i++) {                                                              \
        cur_hex_digit = 2 * cur_hex_digit + (bit_representation[i] == '1'?1:0);                                     \
        printf(" ");                                                                                                \
      }                                                                                                             \
      if (cur_hex_digit <= 9) {                                                                                     \
        printf("%d", cur_hex_digit);                                                                                \
      } else {                                                                                                      \
        printf("%c", 'A' + (cur_hex_digit - 10));                                                                   \
      }                                                                                                             \
    }                                                                                                               \
    printf("| ");                                                                                                   \
  }                                                                                                                 \
                                                                                                                    \
  printf("\n");                                                                                                     \
}

int main() {
  int reading_result, given_number;
  reading_result = scanf("%d", &given_number);
  if (!(reading_result)) 
  {
    printf("Something occured during reading, try running again\n");
  }
  else 
  {
    BITWISE_PRINT(given_number);
  }
  
  return 0;
}
