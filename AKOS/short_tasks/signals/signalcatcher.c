#include <signal.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

static volatile int sign = 0;
volatile const int max_sign = 10;

const int SIGNALS[] = {6,  /* SIGABRT */
                       14, /* SIGALRM */
                       10, /* SIGBUS */
                       18, /* SIGCHLD */
                       25, /* SIGCONT */
                       8,  /* SIGFPE */
                       1,  /* SIGAHUP */
                       4,  /* SIGILL */
                       2,  /* SIGINT */
                       /*9  SIGKILL  we can't overhandle it*/ 
                       13, /* SIGPIPE */
                       3,  /* SIGQUIT */
                       11, /* SIGSEGV */
                       23, /* SIGSTOP */
                       15, /* SIGTERM */
                       20, /* SIGTSTP */
                       26, /* SIGTTIN */
                       27, /* SIGTTOU */
                       16, /* SIGUSR1 */
                       17, /* SIGUSR2 */
                       22, /* SIGPOLL */
                       29, /* SIGPROF */
                       12, /* SIGSYS */
                       5,  /* SIGTRAP */
                       21, /* SIGURG */
                       28, /* SIGVTALRM */
                       30, /* SIGXCPU */
                       31};/* SIGXFSZ */

struct sigaction act;

int isKilling(int sig)
{
  return sig == SIGKILL || sig == SIGTERM || sig == SIGQUIT;
}

void hndlr(int sig, siginfo_t *siginfo, void *context __attribute__((unused)))
{
    pid_t sender_pid = siginfo->si_pid;

    printf("[%d] Signal with number %d", sign + 1, sig);
    sign++;

    if (sig == SIGKILL)
    {
        printf(" (SIGKILL)");
    }
    else if (sig == SIGINT)
    {
        printf(" (SIGINT)");
    }
    else if (sig == SIGQUIT)
    {
        printf(" (SIGQUIT)");
    }
    else if (sig == SIGSTOP)
    {
        printf(" (SIGSTOP)");
    }
    else if (sig == SIGTERM)
    {
        printf(" (SIGTERM)");
    }
    printf(" from %d\n", (int)sender_pid);

    if (isKilling(sig))
    {
      printf("Oops, time to stop\n");
      exit(0);
    }

    return;
}

int main()
{
    size_t i;

    printf("process [%d] started.\n", (int)getpid());

    memset(&act, 0, sizeof(act));
    act.sa_flags |= SA_SIGINFO;
    act.sa_sigaction = hndlr;

    for (i = 0; i < sizeof(SIGNALS) / sizeof(int); i++)
    {
      if (sigaction(SIGNALS[i], &act, NULL) == -1)
      {
        printf("(%d)\n", SIGNALS[i]);
        perror("sigaction set failed");
        return -1;
      }
    }

    while(sign < max_sign)
    {
        pause();
    }

    return 0;
}
