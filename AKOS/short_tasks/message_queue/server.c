#include "misc.h"

void handler(int sig)
{
	if(sig == SIGINT)
	{
		if(msgctl(msgid, IPC_RMID, NULL) == -1)
		{
			perror("msgctl");
			exit(2);
		}
		exit(0);
	}
	signal(SIGINT, handler);
}

int main()
{
	input_data input;
	output_data output;
	key_t key = ftok(PATH, KEY);
	msgid = msgget(key, IPC_CREAT | IPC_EXCL | 0666);
	if(msgid == -1)
	{
		perror("msgget");
		return 1;
	}
	signal(SIGINT, handler);

	while(1)
	{
		msgrcv(msgid, &input, sizeof(input_data), RQUEST, 0);
		output.result = input.a + input.b;
		output.mtype = ANSWER + input.client;
		fprintf(stdout, "in process %ld + %ld = %ld, client = %d\n",
			   	input.a, input.b, output.result, input.client);
		fflush(stdout);
		msgsnd(msgid, &output, sizeof(output_data), 0);
	}
	return 0;
}
