#include "misc.h"

void handler(int sig)
{
	if(sig == SIGINT)
	{
		if(msgctl(msgid, IPC_RMID, NULL) == -1)
		{
			perror("msgctl");
			exit(2);
		}
		exit(0);
	}
	signal(SIGINT, handler);
}

int main()
{
	input_data input;
	output_data output;
	key_t key = ftok(PATH, KEY);
	msgid = msgget(key, 0666);
	if(msgid == -1)
	{
		perror("msgget");
		return 1;
	}
  signal(SIGINT, handler);

	input.client = getpid();
	input.mtype = RQUEST;
	while(1)
	{
		scanf("%ld %ld", &input.a, &input.b);
		msgsnd(msgid, &input, sizeof(input_data), 0);
		msgrcv(msgid, &output, sizeof(output_data), ANSWER + input.client, 0);
		fprintf(stdout, "%ld + %ld = %ld\n", input.a, input.b, output.result);
	}
	return 0;
}
