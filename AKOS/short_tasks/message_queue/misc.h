#ifndef MSGBUF_H
#define MSGBUF_H

#include <sys/types.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#define RQUEST 1
#define ANSWER 2
#define PATH "/bin/ls"
#define KEY '1'

typedef struct
{
	long mtype;
	long a;
	long b;
	pid_t client;
}input_data;

typedef struct
{
	long mtype;
	long result;
}output_data;

int msgid;

#endif /* MSGBUF_H */
