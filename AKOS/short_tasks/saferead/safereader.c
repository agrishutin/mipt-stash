#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <wchar.h>

enum EVALUATION_RESULTS {SUCCESS,
                         FAIL,
                         EMPTY_STRING};

#define wendl (wchar_t)('\n')
#define wspace (wchar_t)(' ')
#define wnullterminator (wchar_t)('\0')
#define wtilda (wchar_t)('~')
#define wtab (wchar_t)('\t')

#define CHUNK 16

size_t linesn = 0;

size_t wstrlen(const wchar_t *s)
{
  size_t ans = 0;
  size_t i;
  for (i = 0; s[i] != wendl && s[i] != wnullterminator; i++)
  {
    ans++;
  }
  return ans;
}

int readString(FILE *in, wchar_t **str)
{
  wchar_t *tmp = NULL;
  wint_t ch;
  size_t len = 0, buf_len = 100;
  size_t i;

  tmp = calloc(buf_len, sizeof(wchar_t));
  if (tmp == NULL)
  {
    perror("calloc");
    return FAIL;
  }
  *str = tmp;
  while (((ch = fgetwc(in)) != WEOF))
  {
    if (ch == wendl || ch == wnullterminator)
    {
      break;
    }
    if (ch == wtab)
    {
      buf_len += 3;
      tmp = realloc(*str, sizeof(wchar_t) * buf_len);
      if (tmp == NULL)
      {
        perror("realloc");
        return FAIL;
      }
      *str = tmp;
      for (i = 0; i < 4; i++)
      {
        (*str)[len] = wspace;
        len++;
      }
    }
    else
    {
      (*str)[len] = (wchar_t)ch;
      len++;
    }

    if(len == buf_len)
    {
      buf_len += CHUNK;
      tmp = realloc(*str, sizeof(wchar_t) * buf_len);
      if(tmp == NULL)
      {
        perror("realloc");
        return FAIL;
      }
      *str = tmp;
    }
  }

  (*str)[len] = wnullterminator;
  len++;

  tmp = realloc(*str, sizeof(wchar_t) * len);
  if (tmp == NULL)
  {
    perror("realloc");
    return FAIL;
  }
  *str = tmp;
  return SUCCESS;
}


int readFile(FILE *in, wchar_t ***text)
{
  wchar_t *cur_str;
  wchar_t **tmp;

  size_t len = 0, buf_len = 100;
  int status;

  tmp = calloc(buf_len, sizeof(wchar_t*));

  if(tmp == NULL)
  {
    perror("calloc");
    return FAIL;
  }
  *text = tmp;

  while (!(feof(in)))
  {
    status = readString(in, &cur_str);
    if (status == FAIL)
    {
      free(cur_str);
      return FAIL;
    }
    (*text)[len] = cur_str;
    len++;
    linesn++;
    if(len == buf_len)
    {
      buf_len += CHUNK;
      tmp = realloc(*text, sizeof(wchar_t*) * buf_len);
      if (tmp == NULL)
      {
        perror("realloc");
        return FAIL;
      }
      *text = tmp;
    }
  }

  tmp = realloc(*text, sizeof(wchar_t*) * len);
  if (tmp == NULL)
  {
    perror("realloc");
    return FAIL;
  }
  *text = tmp;
  return SUCCESS;
}

int main(int argc, char*argv[])
{
  FILE *source;
  wchar_t **content;
  int status;
  size_t i;

  if (argc == 1)
  {
    source = stdin;
  }
  else
  {
    source = fopen(argv[1], "r");
    if (source == NULL)
    {
      perror("source file didn't open correctly");
      return -1;
    }
  }

  status = readFile(source, &content);
  if (status != SUCCESS)
  {
    for (i = 0; i < linesn; i++)
    {
      free(content[i]);
    }
    free(content);
    fclose(source);
    return -1;
  }

  printf("%ld strings in source\n", linesn);
  for (i = 0; i < linesn; i++)
  {
    free(content[i]);
  }
  free(content);
  fclose(source);
  return 0;
}
