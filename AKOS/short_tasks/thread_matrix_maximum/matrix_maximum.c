#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>

enum PTHREAD_STATES {WORKING, FINISHED, READY_TO_JOIN};

struct workspace {
    size_t start, finish;
    int state;
    double max;
};

double** matrix;
size_t matrix_h;
size_t matrix_w;
struct workspace workspaces[5];
pthread_t pthreads[5];

pthread_mutex_t master_mutex;
pthread_cond_t ready_thread;

double matrix_max(double** mtr, size_t h, size_t w)
{
    size_t i, j;
    double ans = **mtr;
    for (i = 0; i < h; ++i)
    {
        for (j = 0; j < w; ++j)
        {
            if (ans < mtr[i][j])
            {
                ans = mtr[i][j];
            }
        }
    }
    return ans;
}

void read_matrix(double*** mtr, size_t* y, size_t* x)
{
    size_t i, j;
    printf("Enter height and width of matrix: ");
    scanf("%ld %ld", y, x);
    *mtr = calloc(sizeof(double*), (*y));

    for (i = 0; i < (*y); ++i)
    {
        (*mtr)[i] = calloc(sizeof(double), (*x));
    }

    printf("Enter values, stored in matrix:\n");
    for (i = 0; i < (*y); ++i)
    {
        for (j = 0; j < (*x); ++j)
        {
            scanf("%lf", &((*mtr)[i][j]));
        }
    }

}

void* thread_body(void* arg_raw)
{
    struct workspace* arg = (void*)arg_raw;
    size_t i, j;
    double result_max = matrix[arg->start][0];

    for (i = arg->start; i < arg->finish; ++i)
    {
        for (j = 0; j < matrix_w; ++j)
        {
            if (result_max < matrix[i][j])
            {
                result_max = matrix[i][j];
            }
        }
    }
    pthread_mutex_lock(&master_mutex);
    arg->max = result_max;
    arg->state = FINISHED;
    pthread_cond_signal(&ready_thread);
    pthread_mutex_unlock(&master_mutex);
    return 0;
}

int main(int argc, char** argv)
{
    size_t i;
    size_t threads_running = 0;
    size_t threads_inited = 0;
    size_t submatrix_h;
    size_t bigger_blocksn;
    size_t cur_h;

    double max;

    if (argc == 2)
    {
        if (freopen(argv[1], "rt", stdin) == NULL)
        {
            perror("freopen");
            return -1;
        }
    }

    read_matrix(&matrix, &matrix_h, &matrix_w);

    pthread_mutex_init(&master_mutex, NULL);
    pthread_cond_init(&ready_thread, NULL);

    submatrix_h = matrix_h / 5;
    bigger_blocksn = matrix_h % 5;
    cur_h = 0;

    for (i = 0; i < 5; ++i)
    {
        workspaces[i].start = cur_h;
        workspaces[i].finish = cur_h + submatrix_h;
        if (i < bigger_blocksn)
        {
          (workspaces[i].finish)++;
        }
        cur_h = workspaces[i].finish;
        if (workspaces[i].start < matrix_h)
        {
            threads_running++;
            threads_inited++;
            workspaces[i].state = WORKING;
            pthread_create(&(pthreads[i]), NULL, thread_body, &(workspaces[i]));
            printf("Thread [%ld] started\n", i);
        } else
        {
            printf("Number of lines is less then 5, so we don't need to run thread[%ld]\n", i);
        }
    }

    while (threads_running > 0)
    {
        int j;
        pthread_mutex_lock(&master_mutex);

        for (j = 0; j < 5; ++j)
        {
            if (workspaces[j].state == FINISHED)
            {
                --threads_running;
                workspaces[j].state = READY_TO_JOIN;
                printf("stopped thread [%d], its maximum is %f\n", j, workspaces[j].max);
            }
        }

        if (threads_running > 0)
        {
            pthread_cond_wait(&ready_thread, &master_mutex);
        }
        pthread_mutex_unlock(&master_mutex);
    }

    pthread_cond_destroy(&ready_thread);
    pthread_mutex_destroy(&master_mutex);

    max = workspaces[0].max;
    for (i = 0; i < threads_inited; ++i)
    {
        assert(workspaces[i].state != WORKING);

        pthread_join(pthreads[i], NULL);
        if (workspaces[i].max > max)
        {
            max = workspaces[i].max;
        }
    }

    for (i = 0; i < matrix_h; ++i)
    {
        free(matrix[i]);
    }
    free(matrix);

    printf("maximal element is %f\n", max);
    return 0;
}
