#include <stdio.h>

enum ReadingErrors 
{
	NoError, ZeroFilePointer, FileReadingError, EndOfProvidedMemory
};

int SafeRead(FILE* file, char** result) 
{
	char cur_char, end_of_read = 0;

	if (file == NULL) 
	{
		return ZeroFilePointer;
	}

	while (!(end_of_read)) 
	{	
		if (ferror(file)) 
		{
			return FileReadingError;
		}
	}

	return NoError;
}

int main() 
{
	return 0;
}
