#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <wchar.h>
#include <unistd.h>

enum EVALUATION_RESULTS {SUCCESS,
                         FAIL,
                         EMPTY_STRING};

#define wendl (wchar_t)('\n')
#define wspace (wchar_t)(' ')
#define wnullterminator (wchar_t)('\0')
#define wtilda (wchar_t)('~')
#define wtab (wchar_t)('\t')

#define CHUNK 16
#define true 1
#define false 0

wchar_t **content;

size_t linesn = 0;
size_t wordsn = 0;
size_t symbolsn = 0;

size_t wstrlen(const wchar_t *s)
{
  return wcslen(s);
}

int readString(FILE *in, wchar_t **str)
{
  wchar_t *tmp = NULL;
  wint_t ch;
  size_t len = 0, buf_len = 100;

  tmp = calloc(buf_len, sizeof(wchar_t));
  if (tmp == NULL)
  {
    perror("calloc");
    return FAIL;
  }
  *str = tmp;
  while (((ch = fgetwc(in)) != WEOF))
  {
    if (ch == wendl)
    {
      break;
    }
    (*str)[len] = (wchar_t)ch;
    len++;

    if(len == buf_len)
    {
      buf_len += CHUNK;
      tmp = realloc(*str, sizeof(wchar_t) * buf_len);
      if(tmp == NULL)
      {
        perror("realloc");
        return FAIL;
      }
      *str = tmp;
    }
  }

  (*str)[len] = wnullterminator;
  len++;

  tmp = realloc(*str, sizeof(wchar_t) * len);
  if (tmp == NULL)
  {
    perror("realloc");
    return FAIL;
  }
  *str = tmp;
  return SUCCESS;
}


int readFile(FILE *in, wchar_t ***text)
{
  wchar_t *cur_str;
  wchar_t **tmp;

  size_t len = 0, buf_len = 100;
  int status;

  tmp = calloc(buf_len, sizeof(wchar_t*));

  if(tmp == NULL)
  {
    perror("calloc");
    return FAIL;
  }
  *text = tmp;

  while (!(feof(in)))
  {
    status = readString(in, &cur_str);
    if (status == FAIL)
    {
      free(cur_str);
      return FAIL;
    }
    (*text)[len] = cur_str;
    len++;
    linesn++;
    if(len == buf_len)
    {
      buf_len += CHUNK;
      tmp = realloc(*text, sizeof(wchar_t*) * buf_len);
      if (tmp == NULL)
      {
        perror("realloc");
        return FAIL;
      }
      *text = tmp;
    }
  }

  tmp = realloc(*text, sizeof(wchar_t*) * len);
  if (tmp == NULL)
  {
    perror("realloc");
    return FAIL;
  }
  *text = tmp;
  return SUCCESS;
}

int isLetter(wchar_t c)
{
  return ((wchar_t)('a') <= c && c <= (wchar_t)'z') ||
         ((wchar_t)('A') <= c && c <= (wchar_t)'Z');
}

/* we judge symbol to be space if it is ' ' or '\t' */
int isSpaceSymbol(wchar_t c)
{
  return (c == wspace || c == wtab);
}

void swap(wchar_t **s1, wchar_t **s2)
{
  wchar_t *tmp = *s2;
  *s2 = *s1;
  *s1 = tmp;
}

int less (const wchar_t *s1, const wchar_t *s2)
{
  size_t len1 = wstrlen(s1), len2 = wstrlen(s2);
  size_t i;
  for (i = 0; i < len1 && i < len2 && (s1[i] == s2[i]); i++)

  if (i == len1)
  {
    return true;
  }
  if (i == len2)
  {
    return false;
  }

  if (s1[i] > s2[i])
  {
    return false;
  }
  else
  {
    return true;
  }
}

void sortStrings()
{
  size_t i;
  for (i = 1; i < linesn; i++)
  {
    size_t j = i;
    while ((j > 0) && less(content[j], content[j - 1]))
    {
      swap(&content[j], &content[j - 1]);
      j--;
    }
  }
}


void usage(char *path)
{
  fwprintf(stderr, L"Usage: %s -s <source> -t <target>\n", path);
}

void count_stats()
{
  size_t i, j;

  for (i = 0; i < linesn; i++)
  {
    size_t cur_len = wstrlen(content[i]);
    wchar_t prev_ch;

    prev_ch = content[i][0];
    if (prev_ch == wendl || prev_ch == wnullterminator)
    {
      continue;
    }

    symbolsn++;
    for (j = 1; j < cur_len; j++)
    {
      if (isLetter(prev_ch) &&  isSpaceSymbol(content[i][j]))
      {
        wordsn++;
      }

      if (content[i][j] != wendl && content[i][j] != wnullterminator)
      {
        symbolsn++;
      }
      prev_ch = content[i][j];
    }
    if (isLetter(prev_ch))
    {
      wordsn++;
    }
  }
}

int main(int argc, char*argv[])
{
  char *sourcepath = NULL, *targetpath = NULL;
  FILE *source, *target;
  char c;
  int status;
  size_t i;


  opterr = 0;
  while ((c = getopt(argc, argv, "s:t:")) != -1)
  {
      switch (c)
        {
        case 's':
          sourcepath = optarg;
          break;
        case 't':
          targetpath = optarg;
          break;
        default:
          usage(argv[0]);
          return 1;
        }
  }

  if (sourcepath == NULL || targetpath == NULL)
  {
    usage(argv[0]);
    return 1;
  }

  source = fopen(sourcepath, "r");
  if (source == NULL)
  {
    perror("source file error");
    return -1;
  }

  status = readFile(source, &content);
  if (status != SUCCESS)
  {
    size_t i;
    for (i = 0; i < linesn; i++)
    {
      free(content[i]);
    }
    free(content);
    fclose(source);
    return -1;
  }

  count_stats();
  wprintf(L"%ld lines\n%ld words\n%ld symbols\n", linesn, wordsn, symbolsn);

  sortStrings();

  target = fopen(targetpath, "w");
  if (target == NULL)
  {
    perror("target file error");
    return -1;
  }

  for (i = 0; i < linesn; i++)
  {
    size_t cur_len = wstrlen(content[i]);
    size_t j = 0;
    for (j = 0; j < cur_len; j++)
    {
      fputwc(content[i][j], target);
    }
    fputwc(wendl, target);
    free(content[i]);
  }
  free(content);
  fclose(source);
  fclose(target);
  return 0;
}
