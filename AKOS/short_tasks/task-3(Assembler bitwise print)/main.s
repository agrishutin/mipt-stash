	.file	"main.c"
	.text
	.globl	bitwise_print
	.type	bitwise_print, @function
bitwise_print:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movl	%edi, -52(%rbp)
	movq	%rsi, -64(%rbp)
	movl	$4, -24(%rbp)
	movl	-24(%rbp), %eax
	sall	$3, %eax
	movl	%eax, -20(%rbp)
	leaq	-52(%rbp), %rax
	movq	%rax, -16(%rbp)
	movl	-24(%rbp), %eax
	sall	$3, %eax
	cltq
	movq	%rax, %rdi
	call	malloc
	movq	%rax, -8(%rbp)
	movl	-24(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -44(%rbp)
	jmp	.L2
.L7:
	movl	$7, -36(%rbp)
	jmp	.L3
.L6:
	movl	-24(%rbp), %eax
	subl	-44(%rbp), %eax
	subl	$1, %eax
	leal	0(,%rax,8), %edx
	movl	$7, %eax
	subl	-36(%rbp), %eax
	addl	%edx, %eax
	movslq	%eax, %rdx
	movq	-8(%rbp), %rax
	addq	%rax, %rdx
	movl	-44(%rbp), %eax
	movslq	%eax, %rcx
	movq	-16(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movl	-36(%rbp), %eax
	movl	%eax, %ecx
	sarl	%cl, %esi
	movl	%esi, %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	.L4
	movl	$49, %eax
	jmp	.L5
.L4:
	movl	$48, %eax
.L5:
	movb	%al, (%rdx)
	subl	$1, -36(%rbp)
.L3:
	cmpl	$0, -36(%rbp)
	jns	.L6
	subl	$1, -44(%rbp)
.L2:
	cmpl	$0, -44(%rbp)
	jns	.L7
	movl	$0, -44(%rbp)
	jmp	.L8
.L13:
	movl	$0, -40(%rbp)
	jmp	.L9
.L12:
	movl	$0, -32(%rbp)
	jmp	.L10
.L11:
	movl	-44(%rbp), %eax
	movslq	%eax, %rdx
	movq	-8(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-64(%rbp), %rax
	movb	%dl, (%rax)
	addq	$1, -64(%rbp)
	addl	$1, -32(%rbp)
	addl	$1, -44(%rbp)
.L10:
	cmpl	$3, -32(%rbp)
	jle	.L11
	movq	-64(%rbp), %rax
	movb	$32, (%rax)
	addq	$1, -64(%rbp)
	addl	$1, -40(%rbp)
.L9:
	cmpl	$1, -40(%rbp)
	jle	.L12
	movq	-64(%rbp), %rax
	movb	$124, (%rax)
	addq	$1, -64(%rbp)
	movq	-64(%rbp), %rax
	movb	$32, (%rax)
	addq	$1, -64(%rbp)
.L8:
	movl	-44(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jl	.L13
	movq	-64(%rbp), %rax
	movb	$10, (%rax)
	addq	$1, -64(%rbp)
	movl	$0, -44(%rbp)
	jmp	.L14
.L21:
	movl	$0, -40(%rbp)
	jmp	.L15
.L20:
	movl	$0, -28(%rbp)
	movl	$0, -32(%rbp)
	jmp	.L16
.L17:
	movl	-28(%rbp), %eax
	leal	(%rax,%rax), %ecx
	movl	-44(%rbp), %eax
	movslq	%eax, %rdx
	movq	-8(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	cmpb	$49, %al
	sete	%al
	movzbl	%al, %eax
	addl	%ecx, %eax
	movl	%eax, -28(%rbp)
	movq	-64(%rbp), %rax
	movb	$32, (%rax)
	addq	$1, -64(%rbp)
	addl	$1, -32(%rbp)
	addl	$1, -44(%rbp)
.L16:
	cmpl	$3, -32(%rbp)
	jle	.L17
	cmpl	$9, -28(%rbp)
	jg	.L18
	movl	-28(%rbp), %eax
	addl	$48, %eax
	movl	%eax, %edx
	movq	-64(%rbp), %rax
	movb	%dl, (%rax)
	addq	$1, -64(%rbp)
	jmp	.L19
.L18:
	movl	-28(%rbp), %eax
	addl	$55, %eax
	movl	%eax, %edx
	movq	-64(%rbp), %rax
	movb	%dl, (%rax)
	addq	$1, -64(%rbp)
.L19:
	addl	$1, -40(%rbp)
.L15:
	cmpl	$1, -40(%rbp)
	jle	.L20
	movq	-64(%rbp), %rax
	movb	$124, (%rax)
	addq	$1, -64(%rbp)
	movq	-64(%rbp), %rax
	movb	$32, (%rax)
	addq	$1, -64(%rbp)
.L14:
	movl	-44(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jl	.L21
	movq	-64(%rbp), %rax
	movb	$10, (%rax)
	addq	$1, -64(%rbp)
	movq	-64(%rbp), %rax
	movb	$0, (%rax)
	addq	$1, -64(%rbp)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	bitwise_print, .-bitwise_print
	.section	.rodata
.LC0:
	.string	"%d"
	.align 8
.LC1:
	.string	"Something occured during reading, try running again"
	.text
	.globl	main
	.type	main, @function
main:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	$99, -24(%rbp)
	leaq	-40(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	%eax, -36(%rbp)
	cmpl	$0, -36(%rbp)
	jne	.L23
	movl	$.LC1, %edi
	call	puts
	jmp	.L24
.L23:
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	malloc
	movq	%rax, -16(%rbp)
	movl	-40(%rbp), %eax
	movq	-16(%rbp), %rdx
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	bitwise_print
	movq	$0, -32(%rbp)
	jmp	.L25
.L26:
	movq	-16(%rbp), %rdx
	movq	-32(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movsbl	%al, %eax
	movl	%eax, %edi
	call	putchar
	addq	$1, -32(%rbp)
.L25:
	movq	-32(%rbp), %rax
	cmpq	-24(%rbp), %rax
	jb	.L26
.L24:
	movl	$0, %eax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L28
	call	__stack_chk_fail
.L28:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.2.1-22ubuntu2) 5.2.1 20151010"
	.section	.note.GNU-stack,"",@progbits
