#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

void bitwise_print(int number, char* buffer)
{
  int i, j, k, cur_bit, cur_hex_digit;
  int bytes_num = sizeof(number), bits = bytes_num * CHAR_BIT;
  unsigned char* charwise = (unsigned char*)(&number);
  char* bit_representation = malloc(bytes_num * CHAR_BIT);

  for (i = bytes_num - 1; i >= 0; i--) {
    for (k = CHAR_BIT - 1; k >= 0; k--) {
      bit_representation[(bytes_num - i - 1) * CHAR_BIT + (CHAR_BIT - k - 1)] = ((charwise[i] & (1 << k))?'1':'0');
    }
  }
  for (i = 0; i < bits;) {
    for (j = 0; j < 2; j++) {
      for (cur_bit = 0; cur_bit < 4; cur_bit++, i++) {
        *buffer = bit_representation[i];
        buffer++;
      }
      *buffer = ' ';
      buffer++;
    }
    *buffer = '|';
    buffer++;
    *buffer = ' ';
    buffer++;
  }
  *buffer = '\n';
  buffer++;

  for (i = 0; i < bits;) {
    for (j = 0; j < 2; j++) {
      cur_hex_digit = 0;
      for (cur_bit = 0; cur_bit < 4; cur_bit++, i++) {
        cur_hex_digit = 2 * cur_hex_digit + (bit_representation[i] == '1'?1:0);
        *buffer = ' ';
        buffer++;
      }
      if (cur_hex_digit <= 9) {
        *buffer = '0' + (cur_hex_digit);
        buffer++;
      } else {
        *buffer = 'A' + (cur_hex_digit - 10);
        buffer++;
      }
    }
    *buffer = '|';
    buffer++;
    *buffer = ' ';
    buffer++;
  }

  *buffer = '\n';
  buffer++;
  *buffer = '\0';
  buffer++;
}

int main() {
  int reading_result, given_number;
  size_t i, ans_size = 99;

  reading_result = scanf("%d", &given_number);
  if (!(reading_result)) 
  {
    printf("Something occured during reading, try running again\n");
  }
  else 
  {
    char* answer = malloc(ans_size);
    bitwise_print(given_number, answer);
    for (i = 0; i < ans_size; i++) {
      printf("%c", answer[i]);
    }
  }
  
  return 0;
}
