/* My process family looks like this:
 *
 * root
 * |
 * |_________________________
 * |       |                |
 * son1   son2            son3
 *         |_______        |
 *         |       |       |
 *         gs1     gs2     gs3
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

int root, son1, son2, son3, grandson1, grandson2, grandson3;
int pipefd[2];

void print_info(pid_t my_pid)
{
    if (my_pid == root)
    {
        printf("*");
    }
    printf("root: %d ", root);
    if (my_pid == son1)
    {
        printf("*");
    }
    printf("son1: %d ", son1);
    if (my_pid == son2)
    {
        printf("*");
    }
    printf("son2: %d ", son2);
    if (my_pid == grandson1)
    {
        printf("*");
    }
    printf("grandson1: %d ", grandson1);
    if (my_pid == grandson2)
    {
        printf("*");
    }
    printf("grandson2: %d ", grandson2);
    if (my_pid == son3)
    {
        printf("*");
    }
    printf("son3: %d ", son3);
    if (my_pid == grandson3)
    {
        printf("*");
    }
    printf("grandson3: %d\n", grandson3);
}

void roothandler1(int sig)
{
    if (sig == SIGUSR1)
    {
        read(pipefd[0], &grandson1, sizeof(pid_t));
        read(pipefd[0], &grandson2, sizeof(pid_t));
    }
}

void roothandler2(int sig)
{
    if (sig == SIGUSR2)
    {
        read(pipefd[0], &grandson3, sizeof(pid_t));
    }
}

void handler1(int sig)
{
    if (sig == SIGUSR1)
    {
        read(pipefd[0], &son2, sizeof(pid_t));
        read(pipefd[0], &grandson1, sizeof(pid_t));
        read(pipefd[0], &grandson2, sizeof(pid_t));
        read(pipefd[0], &son3, sizeof(pid_t));
        read(pipefd[0], &grandson3, sizeof(pid_t));
        print_info(son1);
        write(pipefd[1], &son3, sizeof(pid_t));
        write(pipefd[1], &grandson3, sizeof(pid_t));
        kill(son2, SIGUSR1);
        close(pipefd[0]);
        close(pipefd[1]);
        exit(0);
    }
}

void handler2(int sig)
{
    if (sig == SIGUSR1)
    {
        read(pipefd[0], &son3, sizeof(pid_t));
        read(pipefd[0], &grandson3, sizeof(pid_t));
        print_info(son2);
        write(pipefd[1], &grandson2, sizeof(pid_t));
        write(pipefd[1], &son3, sizeof(pid_t));
        write(pipefd[1], &grandson3, sizeof(pid_t));
        kill(grandson1, SIGUSR1);
        write(pipefd[1], &grandson1, sizeof(pid_t));
        write(pipefd[1], &son3, sizeof(pid_t));
        write(pipefd[1], &grandson3, sizeof(pid_t));
        kill(grandson2, SIGUSR1);
        close(pipefd[0]);
        close(pipefd[1]);
        exit(0);
    }
}

void grandhandler1(int sig)
{
    if (sig == SIGUSR1)
    {
        read(pipefd[0], &grandson2, sizeof(pid_t));
        read(pipefd[0], &son3, sizeof(pid_t));
        read(pipefd[0], &grandson3, sizeof(pid_t));
        print_info(grandson1);
        close(pipefd[0]);
        close(pipefd[1]);
        exit(0);
    }
}

void grandhandler2(int sig)
{
    if (sig == SIGUSR1)
    {
        read(pipefd[0], &grandson1, sizeof(pid_t));
        read(pipefd[0], &son3, sizeof(pid_t));
        read(pipefd[0], &grandson3, sizeof(pid_t));
        print_info(grandson2);
        close(pipefd[0]);
        close(pipefd[1]);
        exit(0);
    }
}

int main()
{
    root = getpid();
    signal(SIGUSR1, roothandler1);
    signal(SIGUSR2, roothandler2);
    if (pipe(pipefd) == -1)
    {
        perror("pipe");
        return -1;
    }
    son1 = fork();
    if (son1 == -1)
    {
        perror("fork");
        return -1;
    }
    if (son1 == 0)
    {
        son1 = getpid();
        signal(SIGUSR1, handler1);
        pause();
    }

    son2 = fork();
    if (son2 == -1)
    {
        perror("fork");
        return -1;
    }
    if (son2 == 0)
    {
        son2 = getpid();
        grandson1 = fork();
        if (grandson1 == -1)
        {
            perror("fork");
            return -1;
        }
        if (grandson1 == 0)
        {
            grandson1 = getpid();
            signal(SIGUSR1, grandhandler1);
            pause();
        }
        grandson2 = fork();
        if (grandson2 == -1)
        {
            perror("fork");
            return -1;
        }
        if (grandson2 == 0)
        {
            grandson2 = getpid();
            signal(SIGUSR1, grandhandler2);
            pause();
        }
        write(pipefd[1], &grandson1, sizeof(pid_t));
        write(pipefd[1], &grandson2, sizeof(pid_t));
        signal(SIGUSR1, handler2);
        kill(root, SIGUSR1);
        pause();
    }

    sleep(10); /* wait for forked process to spawn */

    son3 = fork();
    if (son3 == -1)
    {
        perror("fork");
        return -1;
    }
    if (son3 == 0)
    {
        son3 = getpid();
        grandson3 = fork();
        if (grandson3 == -1)
        {
            perror("fork");
            return -1;
        }
        if (grandson3 == 0)
        {
            grandson3 = getpid();
            print_info(grandson3);
            close(pipefd[0]);
            close(pipefd[1]);
            return 0;
        }
        write(pipefd[1], &grandson3, sizeof(pid_t));
        kill(root, SIGUSR2);
        print_info(son3);
        close(pipefd[0]);
        close(pipefd[1]);
        return 0;
    }

    sleep(10); /* wait for forked process to spawn */

    print_info(root);
    write(pipefd[1], &son2, sizeof(pid_t));
    write(pipefd[1], &grandson1, sizeof(pid_t));
    write(pipefd[1], &grandson2, sizeof(pid_t));
    write(pipefd[1], &son3, sizeof(pid_t));
    write(pipefd[1], &grandson3, sizeof(pid_t));
    kill(son1, SIGUSR1);

}
